% *************************************************************************
%
% function dydt = FlowMap(y, z, p)
% 
% This MATLAB function defines the continuous dynamics of a passive dynamic
% biped in 2D. The models current continuous and discrete states, as well
% as the model parameters are given by the calling routine and the
% derivative of the continuous states is returned. The 'exctFcnHndl'
% describes the inputs to the actuators of the system. If it is not
% provided, a purely passive system is simulated.   
% 
%
% Input:  - A vector of continuous states 'y' 
%         - A vector of discrete states 'z' 
%         - A vector of model system parameters 'p'
% 
% Output: - The derivative of the continuous state vector 'dydt'
%
% Created by C. David Remy on 03/14/2011
% MATLAB 2010a
%
% Documentation:
%  'A MATLAB Framework For Gait Creation', 2011, C. David Remy (1), Keith
%  Buffinton (2), and Roland Siegwart (1),  International Conference on
%  Intelligent Robots and Systems, September 25-30, San Francisco, USA 
%
% (1) Autonomous Systems Lab, Institute of Robotics and Intelligent Systems, 
%     Swiss Federal Institute of Technology (ETHZ) 
%     Tannenstr. 3 / CLA-E-32.1
%     8092 Zurich, Switzerland  
%     cremy@ethz.ch; rsiegwart@ethz.ch
%
% (2) Department of Mechanical Engineering, 
%     Bucknell University
%     701 Moore Avenue
%     Lewisburg, PA-17837, USA
%     buffintk@bucknell.edu
%
%   See also HYBRIDDYNAMICS, JUMPMAP, JUMPSET, COMPUTEDIFFERENTIABLEFORCES 
%            CONTSTATEDEFINITION, DISCSTATEDEFINITION, SYSTPARAMDEFINITION,
%            EXCTSTATEDEFINITION, EXCTPARAMDEFINITION, 
%            VEC2STRUCT, STRUCT2VEC, 
%            SYMBOLICCOMPUTATIONOFEQM. 
%
function dydt = FlowMap(y, ~, p)

    % Get a mapping for the state vector.
    % Keep the index-structs in memory to speed up processing
    persistent contStateIndices
    if isempty(contStateIndices)
        [~,            ~, contStateIndices] = ContStateDefinition();
    end
       
    % Mapping the state-vector to the derivatives ensures a correct vector
    % size
    dydt = y;

    % Map velocities to position derivatives
    dydt(contStateIndices.gamma) = y(contStateIndices.dgamma);
    dydt(contStateIndices.alpha) = y(contStateIndices.dalpha);
    
    % Mass matrix
    M = MassMatrixWrapper(y,p);
    % Graviational and coriolis forces:
    f_cg = F_CoriGravWrapper(y,p);
    % EQM:
    dd_q = M\f_cg;
    
    % Map the generalized accelerations back into continuous state
    % derivatives:
	dydt(contStateIndices.dgamma) = dd_q(1);
    dydt(contStateIndices.dalpha) = dd_q(2);
end
% *************************************************************************
% *************************************************************************
    